/*
Auto-generated by: https://github.com/pmndrs/gltfjsx
author: zisisbad (https://sketchfab.com/zisisbad)
license: CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)
source: https://sketchfab.com/3d-models/earth-36ac223837984d9a841f39edabeefa10
title: Earth
*/

import React, { useRef } from 'react'
import { useGLTF } from '@react-three/drei'

export default function Earth(props) {
  const group = useRef()
  const { nodes, materials } = useGLTF('/earth.gltf')
  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <group ref={group} {...props} dispose={null}>
      <group rotation={[-Math.PI / 2, 0, 0]}>
        <group rotation={[Math.PI / 2, 0, 0]}>
          <mesh
            geometry={nodes.Object_4.geometry}
            material={materials['Material.001']}
          />
        </group>
      </group>
    </group>
  )
}

useGLTF.preload('/earth.gltf')
